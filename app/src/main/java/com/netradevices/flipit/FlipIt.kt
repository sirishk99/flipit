package com.netradevices.flipit

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_flip_it.*

class FlipIt : AppCompatActivity(), SensorEventListener {

    //Global class variables
    var sensorManager:SensorManager? = null
    var gyroSensor:Sensor? = null
    var running = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flip_it)

        //Instantiate SensorManager Class from the system sensor service
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        if (sensorManager == null) {
            Log.i("INFO","Sensor service not available")
        }
        else {
            gyroSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
            if ( gyroSensor == null){
                Toast.makeText(this,"Error getting Gyro Sensor",Toast.LENGTH_LONG).show()
            }
            else
                Toast.makeText(this,"Obtained Gyro Sensor handle", Toast.LENGTH_LONG).show()
        }

    }

    //Mandatory override function
    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        Log.i("INFO","not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //Mandatory override function
    override fun onSensorChanged(event: SensorEvent?) {
        if (running){
            textView.setText("Value" + event!!.values[0])
        }

    }

    // Register the listener for gryo Sensor when application is running on foreground, saving CPU and battery
    override fun onResume() {
        super.onResume()
        running = true

        sensorManager?.registerListener(this, gyroSensor,SensorManager.SENSOR_DELAY_UI)


    }

    // Unregister the listener for gyro Sensor when application is running on background, saving CPU and battery
    override fun onPause() {
        super.onPause()
        running = false
        sensorManager?.unregisterListener(this, gyroSensor)
    }

    // Make the objects null to help in garbage collection

}
